<!--
 ruokin is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 ruokin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with ruokin. If not, see <https://www.gnu.org/licenses/>.
 -->

# Ruokin | Golang

Evaluate soccer referees' performance.

[Vue.js](https://gitlab.com/easbarba/ruokin-vue) | [Main](https://gitlab.com/easbarba/ruokin)

## Stack

- [Go](https://go.dev)
- [httprouter](https://github.com/julienschmidt/httprouter)
- [Testify](https://github.com/stretchr/testify)
- [Koto](https://gitlab.com/easbarba/koto)
- [PostgreSQL](https://www.postgresql.org)
- [Vuejs](https://vuejs.org)
- [Podman](https://podman.io)
- [NGNIX](https://nginx.org)
- [Gitlab CI](https://gitlab.com)
- [Github Actions](https://github.com/features/actions)
- [Git](https://git-scm.com)
- [Debian](https://www.debian.org)
- [GNU](https://www.gnu.org) { [Guix](https://guix.gnu.org), [Emacs](https://www.gnu.org/software/emacs), [Make](https://www.gnu.org/software/make), [Bash](https://www.gnu.org/software/bash), [Coreutils](https://www.gnu.org/software/coreutils), [Guile](https://www.gnu.org/software/guile), ... }

## [Tasks](Makefile)

To speed up development some `make` targets are provided.

| targets                | description                                           |
|------------------------|-------------------------------------------------------|
| up                     | spin up containers to development (synced folders)    |
| down                   | shutdown spinned containers                           |
| image.exec             | run commands inside  development container            |
| image.build            | build development container image                     |
| image.publish          | push to registry current development  container image |
| image.test.integration | run integration tests                                 |
| image.test.unit        | run unit tests                                        |

PS: It relies on an `envs` directory, so be careful to export its content before running its goals.

## [Documentation](docs)

All information about API design, openAPI, and related documentation can be found in the `docs`.

## [Bin Folder](bin)

There are some handy scripts to easily perform daily tasks, check out.

## Development

## [koto](https://gitlab.com/easbarba/koto)

`koto` API Testing tool, just like Postman/Insomnia/Bruno, for CLI power users.

    koto

## [Podman Pods](https://podman.io)

Podman's pod offers a rootless k8s's pods like experience to local development, [check out!](https://developers.redhat.com/blog/2019/01/15/podman-managing-containers-pods#shortcut_to_create_pods)

Check the `Makefile` file for examples on how to use it.

![podman pod](podman_pod.png)

For more information on development check out the `CONTRIBUTING.md` document.

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
