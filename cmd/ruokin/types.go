/*
* ruokin is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ruokin is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ruokin. If not, see <https://www.gnu.org/licenses/>.
**/

package main

import (
	"log"
)

type config struct {
	port int
	env  string
}

type application struct {
	config config
	logger *log.Logger
}

type ResponsetDTO[T any | []any | string | map[string]string] struct {
	Message string `json:"message"`
	Data    T      `json:"data"`
}

func responseDTONew[T any | []any | string | map[string]string](
	status string,
	data T) ResponsetDTO[T] {
	return ResponsetDTO[T]{Message: status, Data: data}
}
