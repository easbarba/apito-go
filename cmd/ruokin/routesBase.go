/*
* ruokin is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ruokin is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ruokin. If not, see <https://www.gnu.org/licenses/>.
**/

package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func baseRoutes(router *httprouter.Router, app *application) {
	router.HandlerFunc(http.MethodGet, "/", app.home)
	router.HandlerFunc(http.MethodGet, "/api", app.openapi)
	router.HandlerFunc(http.MethodGet, "/api/v1", app.hateoas)

	router.HandlerFunc(http.MethodGet, app.toResource("healthcheck"), app.healthcheck)
}
