/*
 * ruokin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ruokin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ruokin. If not, see <https://www.gnu.org/licenses/>.
**/

package main

import (
	"net/http"
)

func (app *application) home(w http.ResponseWriter, r *http.Request) {
	data := responseDTONew("success", "Hello to Ruokin!")
	err := app.writeJSON(w, http.StatusOK, data, nil, http.MethodGet)
	if err != nil {
		app.logger.Println(err)
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) openapi(w http.ResponseWriter, r *http.Request) {
	data := responseDTONew("success", "openAPI!")
	err := app.writeJSON(w, http.StatusOK, data, nil, http.MethodGet)
	if err != nil {
		app.logger.Println(err)
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) hateoas(w http.ResponseWriter, r *http.Request) {
	data := responseDTONew("success", "HATEOAS!")
	err := app.writeJSON(w, http.StatusOK, data, nil, http.MethodGet)
	if err != nil {
		app.logger.Println(err)
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) healthcheck(w http.ResponseWriter, r *http.Request) {
	data := responseDTONew(
		"success",
		map[string]string{
			"environment": app.config.env,
			"version":     version,
		})

	err := app.writeJSON(w, http.StatusOK, data, nil, http.MethodGet)
	if err != nil {
		app.logger.Println(err)
		app.serverErrorResponse(w, r, err)
	}
}
