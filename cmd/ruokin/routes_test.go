/*
 * ruokin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ruokin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ruokin. If not, see <https://www.gnu.org/licenses/>.
**/

package main

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHome(t *testing.T) {
	var cfg config
	cfg.port = 8080
	cfg.env = ""
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime)

	app := &application{
		config: cfg,
		logger: logger,
	}

	ts := httptest.NewServer(app.routes())
	defer ts.Close()

	// ts := serverSetup()

	rs, err := ts.Client().Get(ts.URL + "")
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, rs.StatusCode, http.StatusOK)

	defer rs.Body.Close()
	body, err := io.ReadAll(rs.Body)
	if err != nil {
		t.Fatal(err)
	}
	bytes.TrimSpace(body)

	var rDTO ResponsetDTO[string]
	err = json.Unmarshal(body, &rDTO)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, rDTO.Data, "Hello to Ruokin!")
}
