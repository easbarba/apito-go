/*
* ruokin is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ruokin is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ruokin. If not, see <https://www.gnu.org/licenses/>.
**/

package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

const baseUrl = "/v1"

func (app *application) routes() *httprouter.Router {
	router := httprouter.New()

	router.NotFound = http.HandlerFunc(app.notFoundResponse)
	router.MethodNotAllowed = http.HandlerFunc(app.methodNotAllowedResponse)

	baseRoutes(router, app)

	return router
}

func (app *application) toResource(resource string) string {
	if resource == "" {
		return baseUrl
	}

	// remove heading slash if any
	if len(resource) != 0 && resource[0:1] == "/" {
		resource = resource[1:]
	}

	return baseUrl + "/" + resource
}
