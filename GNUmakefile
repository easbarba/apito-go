# ruokin is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ruokin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ruokin. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: podman, gawk, fzf, guix.

# LOAD ENV FILES
-include envs/.env.*

.DEFAULT_GOAL := test

NAME := ruokin-go
VERSION := $(shell cat .version)
RUNNER ?= podman

BACKEND_IMAGE=${GITLAB_REGISTRY}/${USER}/${NAME}:${VERSION}
BACKEND_FOLDER=/app

# ====================================================== PORCELAIN

.PHONY: up
up: image.initial image.database image.server

.PHONY: down
down:
	${RUNNER} pod rm --force --ignore ${POD_NAME}
	${RUNNER} container rm --force --ignore ${NAME}
	${RUNNER} container rm --force --ignore ${DATABASE_NAME}
	${RUNNER} volume rm --force ${DATABASE_DATA}

.PHONY: test
test: image.test.integration

# ====================================================== CONTAINER

.PHONY: image.initial
image.initial:
	${RUNNER} pod create \
		--publish ${FRONTEND_PORT}:${FRONTEND_INTERNAL_PORT} \
		--publish ${BACKEND_PORT}:${BACKEND_INTERNAL_PORT} \
		--name ${POD_NAME}

.PHONY: image.stats
image.stats:
	${RUNNER} pod stats ${POD_NAME}

.PHONY: image.logs
image.logs:
	${RUNNER} pod logs ${POD_NAME}

.PHONY: image.database
image.database:
	${RUNNER} rm -f ${DATABASE_NAME}
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME} \
		--name ${DATABASE_NAME} \
		--env POSTGRES_PASSWORD=${SQL_PASSWORD} \
		--env POSTGRES_USER=${SQL_USERNAME} \
		--env POSTGRES_DB=${SQL_DATABASE} \
		--volume ${DATABASE_DATA}:${SQL_DATA}:Z \
		${DATABASE_IMAGE}

.PHONY: image.database.repl
image.database.repl:
	${RUNNER} exec -it ${DATABASE_NAME} \
		psql --username ${SQL_USERNAME} --dbname ${SQL_DATABASE}

.PHONY: image.server
image.server:
	${RUNNER} rm -f ${SERVER_NAME}
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--detach \
		--restart=unless-stopped \
		--name ${SERVER_NAME} \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--volume ./ops/nginx.conf:/etc/nginx/conf.d/default.conf:Z \
		${SERVER_IMAGE}

.PHONY: image.prod
image.prod:
	${RUNNER} rm -f ${NAME}-prod
	${RUNNER} run ${RUNNER_STATS} \
		--rm \
		--detach \
		--name ${NAME}-prod \
		--env-file ./envs/.env.db \
		${BACKEND_IMAGE} \
		bash -c 'CompileDaemon --build="go build ./cmd/ruokin" --command="./ruokin" --directory="."'

.PHONY: image.start
image.start:
	${RUNNER} rm -f ${NAME}-start
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-start \
		--env-file ./envs/.env.db \
		--env RUOKIN_VERSION=${VERSION} \
		--env DSN="${SQL_ENGINE_NAME}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}/${SQL_DATABASE}?sslmode=disable" \
		--volume ${PWD}:${BACKEND_FOLDER}/:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash -c 'CompileDaemon --build="go build ./cmd/ruokin" --command="./ruokin" --directory="."'

.PHONY: image.repl
image.repl:
	${RUNNER} rm -f ${NAME}-repl
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-repl \
		--env-file ./envs/.env.db \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash

.PHONY: image.test
image.test:
	${RUNNER} rm -f ${NAME}-test
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-test-unit \
		--env-file ./envs/.env.db \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash -c "go test -v ./..."

.PHONY: image.commands
image.commands:
	${RUNNER} run \
		--rm --tty --interactive \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash -c '$(shell cat container-commands | fzf)'


.PHONY: image.build
image.build:
	${RUNNER} build \
		--file ./Dockerfile \
		--tag ${BACKEND_IMAGE}

.PHONY: image.publish
image.publish:
	${RUNNER} push ${BACKEND_IMAGE}

#======================================================= LOCAL

.PHONY: local.api
local.api:
	koto | jq

.PHONY: local.api.id
local.api.id:
	koto | jq

.PHONY: local.openapi
local.openapi:
	./vendor/bin/openapi src -o docs/openapi/${NAME}-openapi-${VERSION}.yaml

.PHONY: local.guix
local.guix:
	guix shell --pure --container
